;
/*  Resized function
============================================= */
    var waitForFinalEvent = (function () {
        var timers = {};
        return function (callback, ms, uniqueId) {
            if (!uniqueId) {
                uniqueId = "Don't call this twice without a uniqueId";
            }
            if (timers[uniqueId]) {
                clearTimeout (timers[uniqueId]);
            }
            timers[uniqueId] = setTimeout(callback, ms);
        };
    })();


jQuery(document).ready(function($) {

    /*  Window resize function call
    ============================================= */
     $(window).resize(function () {
        waitForFinalEvent(function(){
            //dynamicHeight();
            //topBarContent( $(window).width() );

        }, 500, "some unique string");
    });

    /*  Window scroll function
    ============================================= */
    $(window).scroll(function() {
         var scroll = $(window).scrollTop();

         if (scroll > 200) {
             $('.totop').css('bottom', '15px');
         } else {
             $('.totop').css('bottom', '-30px');
         }
     });

    /*========== Responsive table  ==========*/
    var table = $('.content-area table');
    table.each(function(i,e){
      var e = $(e);
      var newDiv = $('<div class="table-responsive"></div>');
      newDiv.append(e.clone().addClass('table'));
      e.replaceWith(newDiv);
    });

    /* Smootth Scroll
    ============================================= */
    $('.sticky-navigation a, .view-specification, .totop').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
          var target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
          if (target.length) {
            var offset = 0;
            if($(window).width() < 992 ){
                offset = 0;
            }
            $('html,body').animate({
              scrollTop: target.offset().top - offset
            }, 1000);
            return false;
          }
        }
    });


});//READY FUNCTION
